//
//  GuideAppApp.swift
//  GuideApp
//
//  Created by Eugene Kilchevsky on 23.07.2021.
//

import SwiftUI

@main
struct GuideAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
